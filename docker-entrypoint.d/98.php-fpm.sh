#!/bin/bash

Mem=`free -m | awk '/Mem:/{print $2}'`
Swap=`free -m | awk '/Swap:/{print $2}'`
www_conf=/usr/local/etc/php-fpm.d/www.conf

if [ $Mem -le 640 ]; then
  Mem_level=512M
  Memory_limit=64
  THREAD=1
elif [ $Mem -gt 640 -a $Mem -le 1280 ]; then
  Mem_level=1G
  Memory_limit=128
elif [ $Mem -gt 1280 -a $Mem -le 2500 ]; then
  Mem_level=2G
  Memory_limit=192
elif [ $Mem -gt 2500 -a $Mem -le 3500 ]; then
  Mem_level=3G
  Memory_limit=256
elif [ $Mem -gt 3500 -a $Mem -le 4500 ]; then
  Mem_level=4G
  Memory_limit=320
elif [ $Mem -gt 4500 -a $Mem -le 8000 ]; then
  Mem_level=6G
  Memory_limit=384
elif [ $Mem -gt 8000 ]; then
  Mem_level=8G
  Memory_limit=448
fi

if [ $Mem -le 3000 ]; then
  sed -i "s@^pm.max_children.*@pm.max_children = $(($Mem/3/20))@" $www_conf
  sed -i "s@^pm.start_servers.*@pm.start_servers = $(($Mem/3/30))@" $www_conf
  sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = $(($Mem/3/40))@" $www_conf
  sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = $(($Mem/3/20))@" $www_conf
elif [ $Mem -gt 3000 -a $Mem -le 4500 ]; then
  sed -i "s@^pm.max_children.*@pm.max_children = 50@" $www_conf
  sed -i "s@^pm.start_servers.*@pm.start_servers = 30@" $www_conf
  sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 20@" $www_conf
  sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 50@" $www_conf
elif [ $Mem -gt 4500 -a $Mem -le 6500 ]; then
  sed -i "s@^pm.max_children.*@pm.max_children = 60@" $www_conf
  sed -i "s@^pm.start_servers.*@pm.start_servers = 40@" $www_conf
  sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 30@" $www_conf
  sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 60@" $www_conf
elif [ $Mem -gt 6500 -a $Mem -le 8500 ]; then
  sed -i "s@^pm.max_children.*@pm.max_children = 70@" $www_conf
  sed -i "s@^pm.start_servers.*@pm.start_servers = 50@" $www_conf
  sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 40@" $www_conf
  sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 70@" $www_conf
elif [ $Mem -gt 8500 ]; then
  sed -i "s@^pm.max_children.*@pm.max_children = 80@" $www_conf
  sed -i "s@^pm.start_servers.*@pm.start_servers = 60@" $www_conf
  sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 50@" $www_conf
  sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 80@" $www_conf
fi

