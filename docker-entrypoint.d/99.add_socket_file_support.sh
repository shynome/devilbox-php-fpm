#!/bin/sh

add_socket_file (){

  if [ -z "$SOCKET_FILE" ]; then
    return
  fi

  php_fpm_conf=/usr/local/etc/php-fpm.conf
  if [ -z "$NOT_DEBUG_ADD_SOCKET_FILE" ]; then
    php_fpm_conf=php_fpm.conf
  fi

  if [ -z "$SOCKET_FILE_ONLY" ];
  then
    echo "listen         = $SOCKET_FILE" >> $php_fpm_conf
  else
    sed s%'= 9000'%"= $SOCKET_FILE"% -i $php_fpm_conf
  fi

  echo 'listen.mode    = 0777'>> $php_fpm_conf
}

add_socket_file
