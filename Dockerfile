# ARG DEVILBOX_PHP_FPM_TAG=':5.4-prod'
# ARG DEVILBOX_PHP_FPM_TAG='@sha256:060036fd77e41624844a3d827634c7813a08fe4b124bdd29bfce1f67ee340e78'
# ARG DEVILBOX_PHP_FPM_TAG=':5.6-prod'
# ARG DEVILBOX_PHP_FPM_TAG='@sha256:a4d61d34f34a14bd2765f1c9dffe21076fa9b9faf8fdfc5704b984525af13a77'
# ARG DEVILBOX_PHP_FPM_TAG=':7.2-prod'
ARG DEVILBOX_PHP_FPM_TAG='@sha256:789c9758ae80c4151db4127849c2d4bc32dbeedabbd405dd1b841907246e7941'
FROM devilbox/php-fpm${DEVILBOX_PHP_FPM_TAG}

COPY docker-entrypoint.d /docker-entrypoint.d

COPY php-fpm.d/ /usr/local/etc/php-fpm.d/

COPY php.d /usr/local/etc/php/conf.d/

# 监听的 socket 文件
ENV SOCKET_FILE='' \
    SOCKET_FILE_ONLY='' \
    NOT_DEBUG_ADD_SOCKET_FILE='true'
